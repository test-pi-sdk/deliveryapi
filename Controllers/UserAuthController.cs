﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using User.Models;

namespace OrderDeliveryAPI.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserAuthController : ControllerBase
    {
        private readonly UserContext _context;
        private readonly HttpClient _httpClient;

        public UserAuthController(UserContext context)
        {
            _context = context;
            _httpClient = new HttpClient();
        }

        // GET: api/UserAuth
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserAuth>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
            //return StatusCode(200, new { message = connectionString });

        }

        //private bool UserAuthExists(string email)
        //{
        //    return _context.Users.Any(e => e.email == email);
        //}

        [HttpPost]
        [Route("signin")]
        public async Task<ActionResult<UserAuth>> SignIn([FromBody] UserAuth auth)
        {
            try
            {
                if (auth == null)
                {
                    return BadRequest();
                }
                try
                {
                    //using (var response = await _httpClient.GetAsync("https://api.minepi.com/v2/me"))
                    //{
                    //    string apiResponse = await response.Content.ReadAsStringAsync();
                    //    var data = JsonConvert.DeserializeObject<UserAuth>(apiResponse);
                    //    // Access DB here
                    //    //return Ok(new { message = "User signed in" });
                    //}
                    return Ok();
                }
                catch
                {
                    return StatusCode(500, new { message = "Invalid access token" });
                }
            }
            catch
            { throw; }
        }
    }
}

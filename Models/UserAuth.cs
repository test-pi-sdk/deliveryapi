﻿using System.ComponentModel.DataAnnotations;

namespace User.Models
{
    public class UserAuth
    {
        [Key]
        public string email { get; set; }
        public string password { get; set; }
    }
}
